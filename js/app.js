"use strict";

var ocapiStore = angular.module("ocapiStore",['ngRoute']);

ocapiStore.config(['$routeProvider', function ($routeProvider) {

    $routeProvider.when('/GetProduct/:productID', {
        controller: 'getProduct',
        templateUrl: '/views/productView.html'
    })
    .otherwise({ redirectTo: '/' });

}]);

ocapiStore.factory('resourcesFactory', ['$http', function($http){
	var urlBase = 'http://bleblanc-inside-na02-dw.demandware.net/s/SiteGenesis/dw/shop/v15_9/';
	var clientID = 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
	var resourcesFactory = {};

	//set client id header
	$http.defaults.headers.common['dw-client-id']=clientID;
	//$http.defaults.headers.common['Content-Type']='application/json';
	//$http.defaults.headers.common['Access-Control-Request-Origin']='*';

	resourcesFactory.getProduct = function (productID){

		return $http.get(urlBase + 'products/' + productID);
	}

	return resourcesFactory;
}]);

ocapiStore.controller('getProduct', ['$scope','$routeParams','resourcesFactory', function($scope, $routeParams, resourcesFactory){

	$scope.productID = $routeParams.productID;

	getProduct($scope.productID);

	function getProduct(productID){
		resourcesFactory.getProduct(productID)
			.success(function(productJSON){
				$scope.productJSON = productJSON;
			})
			.error(function (error) {
	          $scope.productJSON = 'Unable to retrieve product data: ' + error.message;
	      });
	}
}]);